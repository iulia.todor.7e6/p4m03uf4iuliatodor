﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4M03Uf4IuliaTodor
{
    internal class ProvaData
    {
        public static void OrdenarFechas()
        {
            Console.WriteLine("Introduce el día: ");
            int dia = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce el mes: ");
            int mes = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce el any: ");
            int any = Convert.ToInt32(Console.ReadLine());

            ProvaData pd = new ProvaData();

            if (pd.isCorrectDate(dia, mes, any) == false)
            {
                dia = 1;
                mes = 1;
                any = 1980;
            }

            Data dia1 = new Data(dia, mes, any);
            Data dia2 = new Data(28, 03, 2023);
            

            Console.WriteLine("Sumar dades: ");
            Console.WriteLine(dia1.sumarDades());

            Console.WriteLine("Comparar dades: ");
            Console.WriteLine(dia1.compararDadesMessage(dia1, dia2));

            Console.WriteLine("Días que separan: ");
            Console.WriteLine(dia1.diesQueSeparan(dia1, dia2));

        }
        /// <summary>
        /// Comprueba si la fecha es válida
        /// </summary>
        /// <param name="dia"></param>
        /// <param name="mes"></param>
        /// <param name="any"></param>
        /// <returns></returns>
        public bool isCorrectDate(int dia, int mes, int any)
        {
            if (mes < 1 || mes > 12)
            { return false; }

            else if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
            {
                if (dia > 31 && dia < 1)
                { return false; }

                else
                { return true; }
            }

            else if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
            {
                if (dia > 30 && dia < 1)
                { return false; }

                else
                { return true; }
            }

            else if (mes == 2)
            {
                if (any % 4 == 0 && !(any % 100 == 0) || (any % 400 == 0))
                {
                    if (dia > 29 && dia < 1)
                    { return false; }

                    else
                    { return true; }
                }

                else
                {
                    if (dia > 28 && dia < 1)
                    { return false; }

                    else
                    { return true; }
                }
            }

            else
            { return false; }

        }
    }
}

