﻿using System.Drawing;

namespace P4M03Uf4IuliaTodor
{
    /// <summary>
    /// Clase para los rectángulos
    /// </summary>
    class Rectangle : FiguraGeometrica
    {
        private double @base;
        /// <summary>
        /// Método get de la base
        /// </summary>
        public double GetBase
        {
            get { return this.@base; }
        }
        /// <summary>
        /// Método set de la base
        /// </summary>
        public double SetBase
        {
            set { @base = value; }
        }

        private double altura;

        /// <summary>
        /// Método get de la altura
        /// </summary>
        public double GetAltura
        {
            get { return this.altura; }
        }

        /// <summary>
        /// Método set de la altura
        /// </summary>
        public double SetAltura
        {
            set { altura = value; }
        }

        /// <summary>
        /// Constructor por defecto del rectángulo
        /// </summary>
        public Rectangle() : base()
        {
            this.@base = 1;
            this.altura = 2;
        }

        /// <summary>
        /// Constructor del rectángulo pasado por parámetro
        /// </summary>
        /// <param name="base"></param>
        /// <param name="altura"></param>
        /// <param name="codi"></param>
        /// <param name="nom"></param>
        /// <param name="color"></param>
        public Rectangle(double @base, double altura, int codi, string nom, Color color) : base(codi, nom, color)
        {
            this.@base = @base;
            this.altura = altura;
        }

        /// <summary>
        /// Constructor del rectángulo a partir de una copia
        /// </summary>
        /// <param name="rectangle"></param>
        /// <param name="codi"></param>
        /// <param name="nom"></param>
        /// <param name="color"></param>
        public Rectangle(Rectangle rectangle, int codi, string nom, Color color) : base(codi, nom, color)
        {
            this.@base = rectangle.@base;
            this.altura = rectangle.altura;
        }
        /// <summary>
        /// Método para obtener los datos de la figura
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {

            return "Este rectángulo tiene el código " + this.codi + " y nombre " + nom + " con color " + color + ". Tiene " + @base + " de base y " + altura + " de altura ";
        }

        /// <summary>
        /// Calcula el área del rectángulo
        /// </summary>
        /// <returns></returns>
        public override double Area()
        {
            return @base * altura;
        }
        /// <summary>
        /// Calcula el perímetro del rectángulo
        /// </summary>
        /// <returns></returns>
        public double Perimetre()
        {
            return (@base * 2) + (altura * 2);
        }

    }

}