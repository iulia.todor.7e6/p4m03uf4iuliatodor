﻿using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Xml;

namespace P4M03Uf4IuliaTodor
{
    /// <summary>
    /// Esta es la clase Data, que contiene el día, fecha y año
    /// </summary>
    public class Data : IOrdenable
    {
        protected int dia;
        /// <summary>
        /// Método get de día
        /// </summary>
        public int GetDia
        {
            get { return this.dia; }
        }
        /// <summary>
        /// Método set de día
        /// </summary>
        public int SetDia
        {
            set { dia = value; }
        }
        protected int mes;
        /// <summary>
        /// Método get de mes
        /// </summary>
        public int GetMes
        {
            get { return this.mes; }
        }
        /// <summary>
        /// Método set de mes
        /// </summary>

        public int SetMes
        {
            set { mes = value; }
        }
        protected int any;
        /// <summary>
        /// Método get de año
        /// </summary>
        public int GetAny
        {
            get { return this.any; }
        }
        /// <summary>
        /// Método set de año
        /// </summary>
        public int SetAny
        {
            set { any = value; }
        }

        /// <summary>
        /// Valores de las fechas pasadas por parámetro
        /// </summary>
        /// <param name="dia"></param>
        /// <param name="mes"></param>
        /// <param name="any"></param>
        public Data(int dia, int mes, int any)
        {
            this.dia = dia;
            this.mes = mes;
            this.any = any;
        }

        /// <summary>
        /// Valores predeterminados de la fecha
        /// </summary>
        public Data()
        {
            this.dia = 1;
            this.mes = 1;
            this.any = 1980;
        }
        /// <summary>
        /// Valores de las fechas creadas a partir de una copia
        /// </summary>
        /// <param name="date"></param>
        public Data(Data date)
        {
            this.dia = date.dia;
            this.mes = date.mes;
            this.any = date.any;

        }
        /// <summary>
        /// Suma o resta un número a una fecha dependiendo de si es positivo o negativo
        /// </summary>
        /// <returns></returns>
        public string sumarDades()
        {
            Console.WriteLine("Introduce un número y se lo sumaré o restaré a la fecha");
            int num = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("La suma de la data " + dia + "-" + mes + "-" + any + " y " + num + " es: ");

            if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
            {
                if (num > 0)
                {
                    for (int i = 0; i < num; i++)
                    {
                        dia++;

                        if (dia >= 31)
                        {
                            dia = 0;
                            mes += 1;
                        }

                        if (mes > 12)
                        {
                            mes = 1;
                            any += 1;
                        }
                    }
                }

                else if (num < 0)
                {
                    for (int i = 0; i > num; i--)
                    {
                        dia--;

                        if (dia <= 1)
                        {
                            dia = 31;
                            mes -= 1;
                        }

                        if (mes < 1)
                        {
                            mes = 12;
                            any -= 1;
                        }
                    }

                }
            }

            else if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
            {
                if (num > 0)
                {
                    for (int i = 0; i < num; i++)
                    {
                        dia++;

                        if (dia >= 30)
                        {
                            dia = 0;
                            mes += 1;
                        }

                        if (mes > 12)
                        {
                            mes = 1;
                            any += 1;
                        }
                    }
                }

                else if (num < 0)
                {
                    for (int i = 0; i > num; i--)
                    {
                        dia--;

                        if (dia <= 1)
                        {
                            dia = 30;
                            mes -= 1;
                        }

                        if (mes < 1)
                        {
                            mes = 12;
                            any -= 1;
                        }
                    }

                }
            }

            else if (mes == 2)
            {
                if (num > 0)
                {

                    for (int i = 0; i < num; i++)
                    {
                        dia++;

                        if (any % 4 == 0 && !(any % 100 == 0) || (any % 400 == 0))
                        {
                            if (dia >= 29)
                            {
                                dia = 0;
                                mes += 1;
                            }

                            if (mes > 12)
                            {
                                mes = 1;
                                any += 1;
                            }
                        }

                        else
                        {
                            if (dia >= 28)
                            {
                                dia = 0;
                                mes += 1;
                            }

                            if (mes > 12)
                            {
                                mes = 1;
                                any += 1;
                            }
                        }
                    }
                }

                else if (num < 0)
                {
                    for (int i = 0; i > num; i--)
                    {
                        dia--;

                        if (any % 4 == 0 && !(any % 100 == 0) || (any % 400 == 0))
                        {
                            if (dia <= 1)
                            {
                                dia = 31;
                                mes -= 1;
                            }

                            if (mes < 1)
                            {
                                mes = 12;
                                any -= 1;
                            }
                        }
                    }

                }
            }

            return dia + "-" + mes + "-" + any;
        }
        /// <summary>
        ///  Compara dos fechas y dice si una es mayor, menor o igual que la otra
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public int compararDades(Data date1, Data date2)
        {
            if (date1.any > date2.any)
            {
                return 1;
            }

            else if (date1.any == date2.any)
            {
                if (date1.mes > date2.mes)
                {
                    return 1;
                }

                else if (date1.mes == date2.mes)
                {
                    if (date1.dia > date2.dia)
                    {
                        return 1;
                    }

                    else if (date1.dia == date2.dia)
                    {
                        return 3;
                    }
                }
            }

            return 2;
        }
        /// <summary>
        /// Calcula cuantos días separan dos fechas
        /// </summary>
        /// <param name="date1"></param>
        /// <param name="date2"></param>
        /// <returns></returns>
        public string diesQueSeparan(Data date1, Data date2)
        {
            int diferencia = 0;

            Console.Write("A " + date1.dia + "-" + date1.mes + "-" + date1.any + " y " + date2.dia + "-" + date2.mes + "-" + date2.any + " los separan ");

            int diaContenedor = pasarADias(date1);
            int diaContenedor2 = pasarADias(date2);

            if (compararDades(date1, date2) == 1 || compararDades(date1, date2) == 3)
            {
                diferencia = diaContenedor - diaContenedor2;
            }

            else if (compararDades(date1, date2) == 2)
            {
                diferencia = diaContenedor2 - diaContenedor;
            }

            return diferencia + " dias";
        }
        /// <summary>
        /// Pasa años y meses a días
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public int pasarADias(Data date)
        {
            //Los contenedores guardan los datos para que luego no se modifiquen de forma permanente
            int anyoContenedor = date.any * 365;
            int mesContenedor = 1;

            if (mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12)
            {
                mesContenedor = date.mes * 31;
            }

            else if (mes == 4 || mes == 6 || mes == 9 || mes == 11)
            {
                mesContenedor = date.mes * 30;
            }

            else if (mes == 2)
            {
                if (any % 4 == 0 && !(any % 100 == 0) || (any % 400 == 0))
                {
                    mesContenedor = date.mes * 29;
                }

                else
                {
                    mesContenedor = date.mes * 28;
                }
            }

            return date.dia + anyoContenedor + mesContenedor;
        }
        /// <summary>
        /// Escribe el mensaje según lo que devuelve la función compararDades
        /// </summary>
        /// <param name="dia1"></param>
        /// <param name="dia2"></param>
        /// <returns></returns>
        public string compararDadesMessage(Data dia1, Data dia2)
        {
            if (dia1.compararDades(dia1, dia2) == 1)
            {
                return (dia1.dia + "-" + dia1.mes + "-" + dia1.any) + " es major que " + dia2.dia + "-" + dia2.mes + "-" + dia2.any;
            }

            else if (dia1.compararDades(dia1, dia2) == 2)
            {
                return (dia1.dia + "-" + dia1.mes + "-" + dia1.any) + " es menor que " + dia2.dia + "-" + dia2.mes + "-" + dia2.any;

            }

            else if (dia1.compararDades(dia1, dia2) == 3)
            {
                return (dia1.dia + "-" + dia1.mes + "-" + dia1.any) + " es igual que " + dia2.dia + "-" + dia2.mes + "-" + dia2.any;

            }

            else
            {
                return "Holaaaa";
            }

        }
        /// <summary>
        /// Compara dos objetos de la clase Data
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public int Comparar(IOrdenable x)
        {
            Data y = (Data)x;

            if (compararDades(this, y) == 3)
            {
                return 0;
            }

            else if (compararDades(this, y) == 2)
            {
                return -1;
            }

            else if (compararDades(this, y) == 1)
            {
                return 1;
            }

            return 0;

        }

        /// <summary>
        /// Ordena las fechas
        /// </summary>
        /// <param name="obj"></param>
        public static void Ordenar(IOrdenable[] obj)
        {
            for (int x = 0; x < obj.Length; x++)
            {
                for (int y = 0; y < obj.Length - 1; y++)
                {
                    if (obj[y].Comparar(obj[y + 1]) > obj[y + 1].Comparar(obj[y]))
                    {
                        Data temporal = (Data)obj[y];
                        obj[y] = obj[y + 1];
                        obj[y + 1] = temporal;
                    }
                }
            }
        }



    }

}