﻿using System.Drawing;

namespace P4M03Uf4IuliaTodor
{
    /// <summary>
    /// Clase abstracta para las figuras geométricas
    /// </summary>
    abstract class FiguraGeometrica : IOrdenable
    {
        protected int codi;
        /// <summary>
        /// Método get del código
        /// </summary>
        public int GetCodi
        {
            get { return this.codi; }
        }
        /// <summary>
        /// Método set del código
        /// </summary>

        public int SetCodi
        {
            set { codi = value; }
        }
        protected string nom;

        /// <summary>
        /// Método get del nombre
        /// </summary>
        public string GetNom
        {
            get { return this.nom; }
        }

        /// <summary>
        /// Método set del nombre
        /// </summary>
        public string SetNom
        {
            set { nom = value; }
        }
        protected Color color { get; set; }

        /// <summary>
        /// Método get del color
        /// </summary>
        public Color GetColor
        {
            get { return this.color; }
        }

        /// <summary>
        /// Método set del color
        /// </summary>
        public Color SetColor
        {
            set { color = value; }
        }

        /// <summary>
        /// Método para obtener el área
        /// </summary>
        /// <returns></returns>
        public abstract double Area();

        /// <summary>
        /// Valores por defecto de la Figura Geométrica
        /// </summary>
        public FiguraGeometrica()
        {
            this.codi = 0;
            this.nom = "Defecto";
            this.color = Color.White;
        }

        /// <summary>
        /// Constructor de la figura con parámetros por defecto
        /// </summary>
        /// <param name="codi"></param>
        /// <param name="nom"></param>
        /// <param name="color"></param>
        public FiguraGeometrica(int codi, string nom, Color color)
        {
            this.codi = codi;
            this.nom = nom;
            this.color = color;
        }

        /// <summary>
        /// Constructor de la figura con parámetros como copia de otra figura
        /// </summary>
        /// <param name="figura"></param>
        public FiguraGeometrica(FiguraGeometrica figura)
        {
            this.codi = figura.codi;
            this.nom = figura.nom;
            this.color = figura.color;

        }

        /// <summary>
        ///  Método para imprimir los datos de la figura
        /// </summary>
        /// <returns></returns>
        public override string ToString() //override evita que esto clashee con la funcion ya existente toString
        {
            return "Esta figura tiene " + this.codi + " y nombre " + nom + " con color " + color;
        }

        /// <summary>
        /// Método para comprobar si dos objetos son iguales
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object? obj)
        {
            FiguraGeometrica fg = (FiguraGeometrica)obj;
            return (codi == fg.codi);
        }
        /// <summary>
        /// Método para devolver el código de la figura
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return codi;
        }
        /// <summary>
        /// Método para comparar dos figuras por área
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public int Comparar(IOrdenable x)
        {
            FiguraGeometrica y = (FiguraGeometrica)x;

            if (this.Area() == y.Area())
            {
                return 0;
            }

            else if (this.Area() < y.Area())
            {
                return -1;
            }

            else if (this.Area() > y.Area())
            {
                return 1;
            }

            return 0;

        }
        /// <summary>
        /// Método para ordenar las figuras por área
        /// </summary>
        /// <param name="obj"></param>
        public static void Ordenar(IOrdenable[] obj)
        {
            for (int x = 0; x < obj.Length; x++)
            {
                for (int y = 0; y < obj.Length - 1; y++)
                {
                    if (obj[y].Comparar(obj[y + 1]) > obj[y + 1].Comparar(obj[y]))
                    {
                        FiguraGeometrica temporal = (FiguraGeometrica)obj[y];
                        obj[y] = obj[y + 1];
                        obj[y + 1] = temporal;
                    }
                }
            }
        }

    }

}