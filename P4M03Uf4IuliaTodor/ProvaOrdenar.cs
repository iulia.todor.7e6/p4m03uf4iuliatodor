﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4M03Uf4IuliaTodor
{
    /// <summary>
    /// Clase para probar los métodos Ordenar
    /// </summary>
    public class ProvaOrdenar
    {
        public static void Main(string[] args)
        {
            Data[] fechas = new Data[]
            {
                new Data(28, 03, 2014),
                new Data(28, 03, 2001),
                new Data(3, 04, 2200)

            };

            Data.Ordenar(fechas);

            for (int i = 0; i < fechas.Length; i++)
            {
                Console.WriteLine(fechas[i].GetAny);
            }

            FiguraGeometrica[] figuras = new FiguraGeometrica[]
            {
                new Rectangle(),
                new Rectangle(2, 3, 1, "rectangulo", Color.Aqua),
                new Cercle(),
                new Triangle(),
                new Cercle(),
            };

            FiguraGeometrica.Ordenar(figuras);

            for (int i = 0; i < figuras.Length; i++)
            {
                Console.WriteLine(figuras[i].Area());
            }

            ProvaFigures.ProbarFiguras();
        }
    }
}
