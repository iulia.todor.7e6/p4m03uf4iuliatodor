﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4M03Uf4IuliaTodor
{
    /// <summary>
    /// Clase para las figuras de tipo círculo
    /// </summary>
    class Cercle : FiguraGeometrica
    {
        private double radi;

        /// <summary>
        /// Método get del radio
        /// </summary>
        public double GetRadi
        {
            get { return radi; }
        }

        /// <summary>
        /// Método set del radio
        /// </summary>
        public double SetRadi
        {
            set { radi = value; }
        }
        /// <summary>
        /// Constructor por defecto del círculo
        /// </summary>
        public Cercle() : base()
        {
            this.radi = 1.5;
        }
        /// <summary>
        /// Constructor del círculo con datos pasados por parámetro
        /// </summary>
        /// <param name="radi"></param>
        /// <param name="codi"></param>
        /// <param name="nom"></param>
        /// <param name="color"></param>
        public Cercle(double radi, int codi, string nom, Color color) : base(codi, nom, color)
        {
            this.radi = radi;
        }
        /// <summary>
        /// Constructor del círculo a partir de una copia
        /// </summary>
        /// <param name="cercle"></param>
        /// <param name="codi"></param>
        /// <param name="nom"></param>
        /// <param name="color"></param>
        public Cercle(Cercle cercle, int codi, string nom, Color color) : base(codi, nom, color)
        {
            this.radi = cercle.radi;
        }
        /// <summary>
        /// Método para obtener los datos de la figura
        /// </summary>
        /// <returns></returns>
        public override string ToString() //override evita que esto clashee con la funcion ya existente toString
        {
            return "Este círculo tiene el código " + this.codi + " y nombre " + nom + " con color " + color + ". Tiene " + radi + " de radio";
        }
        /// <summary>
        ///  Método para calcular el área del círculo
        /// </summary>
        /// <returns></returns>
        public override double Area()
        {
            return Math.Round(Math.PI * Math.Pow(radi, 2), 2);
        }
        /// <summary>
        /// Método para calcular el perímetro del círculo
        /// </summary>
        /// <returns></returns>
        public double Perimetre()
        {
            return Math.Round((2 * Math.PI * radi), 2);
        }

    }
}
