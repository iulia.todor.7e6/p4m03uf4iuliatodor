﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4M03Uf4IuliaTodor
{
    /// <summary>
    /// Clase para las figuras de tipo triángulo
    /// </summary>
    class Triangle : FiguraGeometrica
    {
        public double @base;

        /// <summary>
        /// Método get para la base
        /// </summary>
        public double GetBase
        {
            get { return this.@base; }
        }

        /// <summary>
        /// Método set para la base
        /// </summary>
        public double SetBase
        {
            set { @base = value; }
        }

        public double altura;
        /// <summary>
        /// Método get para la altura
        /// </summary>
        public double GetAltura
        {
            get { return this.altura; }
        }

        /// <summary>
        /// Método set para la altura
        /// </summary>
        public double SetAltura
        {
            set { altura = value; }
        }

        /// <summary>
        /// Constructor por defecto para el triángulo
        /// </summary>
        public Triangle() : base()
        {
            this.@base = 2;
            this.altura = 3;
        }

        /// <summary>
        /// Constructor por parámetros del triangulo
        /// </summary>
        /// <param name="base"></param>
        /// <param name="altura"></param>
        /// <param name="codi"></param>
        /// <param name="nom"></param>
        /// <param name="color"></param>
        public Triangle(double @base, double altura, int codi, string nom, Color color) : base(codi, nom, color)
        {
            this.@base = @base;
            this.altura = altura;
        }

        /// <summary>
        /// Constructor del triangulo a partir de una copia
        /// </summary>
        /// <param name="triangle"></param>
        /// <param name="codi"></param>
        /// <param name="nom"></param>
        /// <param name="color"></param>
        public Triangle(Triangle triangle, int codi, string nom, Color color) : base(codi, nom, color)
        {
            this.@base = triangle.@base;
            this.altura = triangle.altura;

        }
        /// <summary>
        /// Metodo para obtener los datos de la figura
        /// </summary>
        /// <returns></returns>
        public override string ToString() //override evita que esto clashee con la funcion ya existente toString
        {
            return "Este triángulo tiene el código " + this.codi + " y nombre " + nom + " con color " + color + ". Tiene " + @base + " de base y " + altura + " de altura ";
        }
        /// <summary>
        /// Calcula el área del triángulo
        /// </summary>
        /// <returns></returns>
        public override double Area()
        {
            return (@base * altura) / 2;
        }
        /// <summary>
        /// Calcula el perímetro del triángulo asumiendo que este es isósceles
        /// </summary>
        /// <returns></returns>
        public double Perimetre()
        {
            double hipotenusa = Math.Sqrt(Math.Pow((@base / 2), 2) + Math.Pow(altura, 2));

            return Math.Round((@base + (hipotenusa * 2)), 2);
        }
    }
}
