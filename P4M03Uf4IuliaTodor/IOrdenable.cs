﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4M03Uf4IuliaTodor
{
    /// <summary>
    /// Interfaz IOrdenable
    /// </summary>
    public interface IOrdenable
    {
        /// <summary>
        /// Método para comparar objetos de la clase
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        int Comparar(IOrdenable x);
        
    }

}
