﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P4M03Uf4IuliaTodor
{    /// <summary>
     /// Clase para probar las figuras
     /// </summary>
    public class ProvaFigures
    {
        /// <summary>
        /// Método donde se prueban las figuras
        /// </summary>
        public static void ProbarFiguras()
        {
            //Declaramos estos dos rectángulos y comprobamos si son iguales.
            Rectangle rectangle = new Rectangle(2, 3, 1, "rectangulo", Color.Aqua);//Código 1
            Rectangle rectangle2 = new Rectangle(2, 3, 1, "rectangulo", Color.Aqua);//Código 1
            //Devuelve true
            Console.WriteLine("Son iguales? " + rectangle.Equals(rectangle2));
            Console.WriteLine(rectangle.GetHashCode());

            //Ahora el código es 3
            rectangle.SetCodi = 3;
            //Devuelve false
            Console.WriteLine("Son iguales? " + rectangle.Equals(rectangle2));
            Console.WriteLine(rectangle.GetHashCode());
            Console.WriteLine(rectangle.GetCodi);

            Triangle triangulo = new Triangle();

            Cercle circulo = new Cercle();

            Console.WriteLine(rectangle.ToString());
            Console.WriteLine("El área del rectángulo es: " + rectangle.Area());
            Console.WriteLine("El perímetro del rectángulo es: " + rectangle.Perimetre());
            Console.WriteLine();

            Console.WriteLine(triangulo.ToString());
            Console.WriteLine("El área del triangulo es: " + triangulo.Area());
            Console.WriteLine("El perímetro del triangulo es: " + triangulo.Perimetre());
            Console.WriteLine();

            Console.WriteLine(circulo.ToString());
            Console.WriteLine("El área del círculo es: " + circulo.Area());
            Console.WriteLine("El perímetro del círculo es: " + circulo.Perimetre());
        }
    }
}
